/* MainActivity for user profile
 * Copyright (C) 2020  Ngô Ngọc Đức Huy
 * This file is part of GenWord.
 *
 * GenWord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GenWord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with GenWord.  If not, see <https://www.gnu.org/licenses/>.
 */

package ht.sr.huyngo.genword

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.util.Log

import androidx.appcompat.app.AppCompatActivity

import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText



class MainActivity : AppCompatActivity() {
    val categories : Map<String, List<String>>
        get() {
            val input = findViewById<TextInputEditText>(R.id.categories_input).text
            val cates = input!!.split("\n").associate {
                val (left, right) = it.split("=")
                left to right.split("/")
            }
            // TODO: Handle if the input is incorrect format
            return cates
        }
    val pattern : String
        get() = findViewById<TextInputEditText>(R.id.pattern_input).text.toString()
    val times : Int
        get() = findViewById<TextInputEditText>(R.id.n_words_input).text.toString().toInt()
    val forbiddenRules : List<Regex>
        get() = findViewById<TextInputEditText>(R.id.forbidden_input).text.toString().split("\n").map{it.toRegex()}
    val rewriteRules : Map<Regex, String>
        get() {
            val input = findViewById<TextInputEditText>(R.id.rewrite_input).text
            val rules = input!!.split("\n").associate {
                val (left, right) = it.split("=")
                left.toRegex() to right
            }
            // TODO: Handle if the input is incorrect format
            return rules
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)
        setSupportActionBar(findViewById(R.id.toolbar))
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title
        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_scrolling, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun rewrite(str: String): String {
        var newStr: String = str
        for ((pattern, rewrite) in rewriteRules) {
            newStr = newStr.replace(pattern, rewrite)
        }
        return newStr
    }

    fun generateWord(): String = pattern.map{
        categories[it.toString()]!!.random()
    }.joinToString("")

    /**
     * Check [word] if it's forbidden according to forbiddenRules.
     */
    fun isForbidden(word: String): Boolean {
        for (rule in forbiddenRules) {
            rule.find(word)?.let{
                return true
            }
        }
        return false
    }

    /* Get a valid new word. */
    fun getNewWord(): String {
        var word: String
        do {
            word = generateWord()
        } while (isForbidden(word))
        return rewrite(word)
    }

    /* Generate a new word based on the parameters.*/
    public fun generate(view: View) {
        var s : String = ""
        for (i in 1..times)
            s += getNewWord() + "\n"
        findViewById<TextView>(R.id.output).text = s
    }
}
