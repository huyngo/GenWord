# Contribution Guidelines

## Architecture

TBD

## Coding convention

## Kotlin
## XML

Please sort the attributes as follow:

- ID of a View is always on top, unless the namespace is defined, in which case ID comes right after the namespace definition.
- Right after the ID is its content, if applicable (e.g. `android:text` for `TextView`, `android:src` for `ImageView`).
- Event listeners (e.g. `android:onClick`) are put at the end.
- All other attributes are sorted alphabetially.
