# GenWord - Word generator for Android

Word generator is a tool that helps conlangers creating vocabulary
and reduce the boring labor-intensive task. There have been several
tools for such purpose, such as [Awkword][awkword] or [Zompist Gen][zompist].
However, considering that they are all [SaaSS][saass] (Service as a Software Substitute,
online services serving purposes that can be perfectly achieved by an offline software)
or [JavaScript web app][js-trap], they unnecessarily force users
to be online to be able to generate words.
There are occasions users prefer to be offline, such as when they
ride on a bus. A phone would be more often used in such case, which is why I made this
as an Android app.

## How to run

[Android's document][android] gives detailed instruction of
how to build run Android apps from source code. It also includes instruction
for command line build in case you don't want to install Android Studio.
You need Gradle 4.0.1 and Kotlin 1.3.72 or later for the build.

## To-do list

A check list for development tasks.

### Documentation

- [ ] CONTRIBUTING.md
- [ ] Installation instruction
- [ ] Architecture
- [ ] Usage

### Functionalities

- [X] Generate words
- [X] Add rewrite rules
- [X] Add forbidden rules
- [ ] Handle multi pattern
- [ ] Check carefully the input
	- [ ] `categories`
	- [ ] `pattern`
	- [ ] `number`
	- [ ] `rewriteRules`

### Quality Assurance

- [ ] `MainActivity`
	- [ ] `categories`
	- [ ] `pattern`
	- [ ] `time`
	- [ ] `generateWord()`
	- [ ] `generate()`
	- [ ] `rewrite()`

[android]: https://developer.android.com/studio/run
[awkword]: http://akana.conlang.org/tools/awkwords/
[zompist]: http://www.zompist.com/gen.html
[saass]: https://www.gnu.org/philosophy/who-does-that-server-really-serve.html
[js-trap]: https://www.gnu.org/philosophy/javascript-trap.html
